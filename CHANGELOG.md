<a name="1.1.0-alpha.0"></a>
# [1.1.0-alpha.0](https://gitlab.com/Lucky-JobDone/example-project/compare/1.0.1-alpha.2...1.1.0-alpha.0) (2020-04-21)


### Bug Fixes

* **gitlab-ci:** added back git config ([37174ef](https://gitlab.com/Lucky-JobDone/example-project/commit/37174ef))
* **release-it:** git commit: false ([6c7cab4](https://gitlab.com/Lucky-JobDone/example-project/commit/6c7cab4))
* **release-it:** ignoreVersion in npm ([d02fd6f](https://gitlab.com/Lucky-JobDone/example-project/commit/d02fd6f))


### Features

* **gitlab-ci:** added release-it ([a75c0f1](https://gitlab.com/Lucky-JobDone/example-project/commit/a75c0f1))
* **readme:** test a feat release ([520a7b9](https://gitlab.com/Lucky-JobDone/example-project/commit/520a7b9))
* **release-it:** updated config ([2c5484e](https://gitlab.com/Lucky-JobDone/example-project/commit/2c5484e))



<a name="1.0.1-alpha.2"></a>
## [1.0.1-alpha.2](https://gitlab.com/Lucky-JobDone/example-project/compare/1.0.1-alpha.1...1.0.1-alpha.2) (2020-04-17)



<a name="1.0.1-alpha.1"></a>
## [1.0.1-alpha.1](https://gitlab.com/Lucky-JobDone/example-project/compare/1.0.1-alpha.0...1.0.1-alpha.1) (2020-04-17)



<a name="1.0.1-alpha.0"></a>
## [1.0.1-alpha.0](https://gitlab.com/Lucky-JobDone/example-project/compare/ffc5c44...1.0.1-alpha.0) (2020-04-17)


### Bug Fixes

* **gitlab-ci:** added chmod fix ([71d1c97](https://gitlab.com/Lucky-JobDone/example-project/commit/71d1c97))
* **gitlab-ci:** added cut ([3599b0a](https://gitlab.com/Lucky-JobDone/example-project/commit/3599b0a))
* **gitlab-ci:** added cut 2 ([16c89e4](https://gitlab.com/Lucky-JobDone/example-project/commit/16c89e4))
* **gitlab-ci:** added git checkout $CI_BUILD_REF_NAME ([0c9b306](https://gitlab.com/Lucky-JobDone/example-project/commit/0c9b306))
* **gitlab-ci:** added git checkout to correct job ([0c4e559](https://gitlab.com/Lucky-JobDone/example-project/commit/0c4e559))
* **gitlab-ci:** added git config ([6535758](https://gitlab.com/Lucky-JobDone/example-project/commit/6535758))
* **gitlab-ci:** added git dummy user ([da6a213](https://gitlab.com/Lucky-JobDone/example-project/commit/da6a213))
* **gitlab-ci:** added git remote set-url ssh ([178aa5e](https://gitlab.com/Lucky-JobDone/example-project/commit/178aa5e))
* **gitlab-ci:** added head ([2b3d005](https://gitlab.com/Lucky-JobDone/example-project/commit/2b3d005))
* **gitlab-ci:** added manual gitlab ref ([f3473e5](https://gitlab.com/Lucky-JobDone/example-project/commit/f3473e5))
* **gitlab-ci:** added missing slash ([a104ef3](https://gitlab.com/Lucky-JobDone/example-project/commit/a104ef3))
* **gitlab-ci:** added pwd and ls -la ([30ad860](https://gitlab.com/Lucky-JobDone/example-project/commit/30ad860))
* **gitlab-ci:** added release-it to gitlab-ci.yml ([eb5c670](https://gitlab.com/Lucky-JobDone/example-project/commit/eb5c670))
* **gitlab-ci:** added ssh key to pipeline ([841952a](https://gitlab.com/Lucky-JobDone/example-project/commit/841952a))
* **gitlab-ci:** added ssh to alpine ([682c9d5](https://gitlab.com/Lucky-JobDone/example-project/commit/682c9d5))
* **gitlab-ci:** adding pit pull after setting head ([a9724bc](https://gitlab.com/Lucky-JobDone/example-project/commit/a9724bc))
* **gitlab-ci:** changed name ([6b47a8e](https://gitlab.com/Lucky-JobDone/example-project/commit/6b47a8e))
* **gitlab-ci:** changed order ([c86c583](https://gitlab.com/Lucky-JobDone/example-project/commit/c86c583))
* **gitlab-ci:** changed to id_ed25519 format ([5c3acc2](https://gitlab.com/Lucky-JobDone/example-project/commit/5c3acc2))
* **gitlab-ci:** changed to ssh user ([82fc25c](https://gitlab.com/Lucky-JobDone/example-project/commit/82fc25c))
* **gitlab-ci:** changed url ([42208f6](https://gitlab.com/Lucky-JobDone/example-project/commit/42208f6))
* **gitlab-ci:** config more ([5f6b919](https://gitlab.com/Lucky-JobDone/example-project/commit/5f6b919))
* **gitlab-ci:** fingerprint check ([f2076f2](https://gitlab.com/Lucky-JobDone/example-project/commit/f2076f2))
* **gitlab-ci:** first install git ([18841ab](https://gitlab.com/Lucky-JobDone/example-project/commit/18841ab))
* **gitlab-ci:** moved git pull to gitlab-ci ([b610bf0](https://gitlab.com/Lucky-JobDone/example-project/commit/b610bf0))
* **gitlab-ci:** removed git pull for testing purposes ([73cb8f6](https://gitlab.com/Lucky-JobDone/example-project/commit/73cb8f6))
* **gitlab-ci:** reverted back to git pull origin X ([548d62f](https://gitlab.com/Lucky-JobDone/example-project/commit/548d62f))
* **gitlab-ci:** rewrite file ([d1a7442](https://gitlab.com/Lucky-JobDone/example-project/commit/d1a7442))
* **gitlab-ci:** specified location of git pull ([c0ffae6](https://gitlab.com/Lucky-JobDone/example-project/commit/c0ffae6))
* **gitlab-ci:** switched order of validation ([18dd024](https://gitlab.com/Lucky-JobDone/example-project/commit/18dd024))
* **README:** test ([01cd5ce](https://gitlab.com/Lucky-JobDone/example-project/commit/01cd5ce))
* **README:** test1 ([521ef35](https://gitlab.com/Lucky-JobDone/example-project/commit/521ef35))
* **release-it:** added ci mode ([535ad34](https://gitlab.com/Lucky-JobDone/example-project/commit/535ad34))
* **release-it:** added git checkout ([6e2a49e](https://gitlab.com/Lucky-JobDone/example-project/commit/6e2a49e))
* **release-it:** added npm publish false ([a9e7ad5](https://gitlab.com/Lucky-JobDone/example-project/commit/a9e7ad5))


### Features

* **app:** initial commit ([ffc5c44](https://gitlab.com/Lucky-JobDone/example-project/commit/ffc5c44))

